@rem Script file to compile lfann with lua-5.1.4 and fann-2.1.0 to produce lfann.dll
@rem to work with lua51.dll & fanndouble.dll.
@rem
@rem Edit LUA51SRC and FANNDOUBLESRC as required.
@rem
@rem See README for requirements to build fanndouble.dll.

@setlocal
@set MYCOMPILE=cl /nologo /MD /O2 /W3 /c /D_CRT_SECURE_NO_DEPRECATE /wd4244
@set LUA51SRC=..\..\lua-5.1.4\src
@set FANNDOUBLESRC=..\..\fann-2.1.0\src
@set MYLINK=link /nologo
@set MYMT=mt /nologo

cd ..\src
%MYCOMPILE% /I%LUA51SRC% /I%FANNDOUBLESRC%\include interface.c
%MYLINK% /DLL /out:lfann.dll *.obj %LUA51SRC%\lua51.lib %FANNDOUBLESRC%\..\MicrosoftWindowsDll\bin\fanndouble.lib
if exist lfann.dll.manifest^
  %MYMT% -manifest lfann.dll.manifest -outputresource:lfann.dll;2
del *.obj *.manifest
cd ..\WindowsDll
